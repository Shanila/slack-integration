import React, { Component } from 'react';
import logo from './assets/img/loop-11-logo.svg';
import camera from './assets/img/video-camera.png';
import Modal from 'simple-react-modal';
import './assets/css/App.css';
import GetChannelList from './ajax/GetChannelList';
import { Alert } from 'reactstrap'


class App extends Component{

    state = {
        modalShow: false,
        collectVal: '',
        alertVisible: false
    };

    //Modal control functions
    modalShow = () =>{
        this.setState({modalShow: true})
    }

    modalClose = () =>{
        this.setState({modalShow: false})
        this.setState({collectVal:""})
    }

    //Alert controls
    toggleAlert = () =>{
        this.setState({alertVisible: !this.state.alertVisible})
        console.log("toggleAlert");
    }

    //Data Collection from Modal
    collectValue = (e) =>{
        this.setState({collectVal: e.target.value})
    }

    //Form submit
    handleSubmit = (event) => {
        //If collectVal is not defined then proceed
        var postData = {channel: this.state.collectVal};
        event.preventDefault();

        fetch("https://test-api.loop11.com/v1/slack/",{
            method: 'post',
            headers: {
              "Content-type": "text/plain; charset=UTF-8"
            },
            body: JSON.stringify(postData)
        })
        .then(
          (response) => {
              this.modalClose();
              this.toggleAlert();
            },
          // Note: it's important to handle errors here
          // instead of a catch() block so that we don't swallow
          // exceptions from actual bugs in components.
          (error) => {
            this.setState({
              isLoaded: true,
              error
            });
          }
        );
    }

    render(){
        return (
            <div id="App" className="App">
                <Alert color="success" isOpen={this.state.alertVisible} fade={false} toggle={this.toggleAlert.bind(this)}>
                  Video clip shared with slack!
                </Alert>
                <header className="App-header">
                    <img src={logo} className="App-logo" alt="logo" />
                </header>

                <div className="App-body">
                    <button onClick={this.modalShow.bind(this)}>Click</button>
                    <Modal
                        containerClassName="shareModal"
                        containerStyle={{padding: '1px'}}
                        closeOnOuterClick={true}
                        show={this.state.modalShow}
                        onClose={this.modalClose.bind(this)}>

                        <header>
                            <h3>Share video clip <a className="closeBtn" onClick={this.modalClose.bind(this)}>x</a></h3>
                        </header>
                        <div className="modalBody">
                            <strong className="bodyTitle">
                              Select slack channel
                            </strong>
                            <p>
                              To share this clip, add email addresses separated by commas, then click 'Send'.
                            </p>
                            <p>
                              <span className="filePath">Projects0001 Report / Task 2 / </span> <span>Participant 4</span><br/>
                              <img src={camera}/><span>Start</span> <span className="videoTiming">2:30</span>    <span>End</span> <span className="videoTiming">2:30</span>
                            </p>
                            <form onSubmit={this.handleSubmit}>
                                  <div className="slackChannelListing">
                                    <GetChannelList passValuetoParent={e => this.collectValue(e)}/>
                                  </div>
                                  <div className="shareWithSlack">
                                    <input type="submit" value="Share with Slack" disabled={this.state.collectVal?'':'disabled'}/>
                                  </div>
                            </form>
                        </div>

                    </Modal>
                </div>
            </div>
        );
    }
}

export default App;