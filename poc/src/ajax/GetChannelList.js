import React, { Component } from 'react';

class GetChannelList extends Component {
    state = {
        error: null,
        isLoaded: false,
        channels: [],
        selectValue: ''
    };

    componentDidMount() {
        fetch("https://test-api.loop11.com/v1/slack/")
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        channels: result.channels
                    });
                },
                // Note: it's important to handle errors here
                // instead of a catch() block so that we don't swallow
                // exceptions from actual bugs in components.
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )
    }

    render() {

        const { error, isLoaded, channels } = this.state;
        if (error) {
            return <div>Error: {error.message}</div>;
        } else if (!isLoaded) {
            return <div>Loading...</div>;
        } else {
            return (
                <div>
                    <select defaultValue={this.state.selectValue} onChange={this.props.passValuetoParent}>
                      <option value="" disabled>Select</option>
                        <optgroup label="Channels">
                            {channels.map((channel, i) => (
                                <option key={i} value={channel.name}>
                                    {channel.name}
                                </option>
                            ))}
                      </optgroup>
                    </select>
                </div>
            );
        }
    }
}


export default GetChannelList;
